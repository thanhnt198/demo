import { createWebHistory, createRouter } from "vue-router";
import { RouteRecordRaw } from "vue-router";

const routes: Array<RouteRecordRaw> = [
  // {
  //   path: "/",
  //   alias: "/product-list",
  //   name: "productList",
  //   component: () => import("@/components/Product.vue"),
  // },
  {
    path: "/",
    alias: "/home-page",
    name: "homepage",
    component: () => import("@/pages/HomePage.vue"),
  },
  {
    path: "/product/:id",
    name: "productDetail",
    component: () => import("./pages/Detail.vue"),
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
